package util;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.core.requests.restaction.MessageAction;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class Util {

    private static EmbedBuilder eb = new EmbedBuilder();
//    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss z GMT+2");

    public enum LogLevel{
        ERROR, LOG, INFO
    }

    public static String getTime(){
        long millis = System.currentTimeMillis();
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;
        if (hour+2 == 24){
            hour = 0;
        }else if (hour+2 == 25){
            hour = 1;
        }else if (hour+2 == 26){
            hour = 2;
        }

        return String.format("%02d:%02d:%02d", hour+2, minute, second);
    }

    public static void log(String msg, LogLevel loglevel, Class... c){
        String log = "";

        if(loglevel.equals(LogLevel.ERROR)){
            log = "ERROR";
        }else if(loglevel.equals(LogLevel.LOG)){
            log = "LOG";
        }else if(loglevel.equals(LogLevel.INFO)){
            log = "INFO";
        }

        if(c.length > 0){
            System.out.println("[" + getTime() + "][" + log + "] " +
                    c[0].getName() + " " + msg);
        }else{
            System.out.println("[" + getTime() + "][" + log + "] " + msg);
        }


    }

    public static void log(String server, String msg, LogLevel loglevel, Class... c){
        String log = "";

        if(loglevel.equals(LogLevel.ERROR)){
            log = "ERROR";
        }else if(loglevel.equals(LogLevel.LOG)){
            log = "LOG";
        }else if(loglevel.equals(LogLevel.INFO)){
            log = "INFO";
        }

        if(c.length > 0){
            System.out.println("[" + getTime() + "][" + log + "][Server: " + server + "] " +
                    c[0].getName() + " " + msg);
        }else{
            System.out.println("[" + getTime() + "][" + log + "][Server: " + server + "] " + msg);
        }


    }

    /**
     *
     * @param channel TextChannel channel.
     * @param msg String message.
     */
    public static void message(TextChannel channel, String msg){
        if(msg.length() != 0){
            eb.setDescription(msg);
        }else{
            eb.setDescription("NO MESSAGE PROVIDED");
        }
        try{
            channel.sendMessage(msg).queue();
        }catch (InsufficientPermissionException e){
            log(channel.getGuild().getName(), e.toString(), LogLevel.ERROR);
            //event.getTextChannel().sendMessage("**I NEED MORE PERMISSIONS!** Needed permission: EMBED_LINKS").complete();
        }

        eb.clear();
    }

    /**
     *
     * @param channel Textchannel.
     * @param msg String message.
     * @param color Color color.
     */
    public static void message(TextChannel channel, String msg, Color color){
        if(color != null){
            eb.setColor(color);
        }else{
            eb.setColor(Color.pink);
        }

        if(msg.length() != 0){
            eb.setDescription(msg);
        }else{
            eb.setDescription("NO MESSAGE PROVIDED");
        }
        try{
            channel.sendMessage(eb.build()).queue();
        }catch (InsufficientPermissionException e){
            log(channel.getGuild().getName(), e.toString(), LogLevel.ERROR);
            //event.getTextChannel().sendMessage("**I NEED MORE PERMISSIONS!** Needed permission: EMBED_LINKS").complete();
        }

        eb.clear();
    }

    /**
     *
     * @param channel TextChannel event.
     * @param msg String message.
     * @param color Color color.
     */
    public static void message(TextChannel channel, String msg, Color color, String title, String footer){
        if(color != null){
            eb.setColor(color);
        }else{
            eb.setColor(Color.pink);
        }

        if(msg.length() != 0){
            eb.setDescription(msg);
        }else{
            eb.setDescription("NO MESSAGE PROVIDED");
        }

        if(title.length() != 0){
            eb.setTitle(title);
        }

        eb.setFooter("[" + getTime() + "] " + footer + " • provided by ArcticonSnipeBot", null);

        try{
            channel.sendMessage(eb.build()).queue();
        }catch (InsufficientPermissionException e){
            log(channel.getGuild().getName(), e.toString(), LogLevel.ERROR);
            //event.getTextChannel().sendMessage("**I NEED MORE PERMISSIONS!** Needed permission: EMBED_LINKS").complete();
        }
        eb.clear();
    }

    /**
     *
     * @param channel TextChannel event.
     * @param msg String message.
     * @param color Color color.
     */
    public static void editMessage(TextChannel channel, String messageID, String msg, Color color, String title, String footer){
        if(color != null){
            eb.setColor(color);
        }else{
            eb.setColor(Color.pink);
        }

        if(msg.length() != 0){
            eb.setDescription(msg);
        }else{
            eb.setDescription("NO MESSAGE PROVIDED");
        }

        if(title.length() != 0){
            eb.setTitle(title);
        }

        eb.setFooter("[" + getTime() + "] " + footer + " • provided by ArcticonSnipeBot", null);

        try{
            channel.editMessageById(messageID, eb.build()).queue();
        }catch (InsufficientPermissionException e){
            log(channel.getGuild().getName(), e.toString(), LogLevel.ERROR);
            //event.getTextChannel().sendMessage("**I NEED MORE PERMISSIONS!** Needed permission: EMBED_LINKS").complete();
        }catch (ErrorResponseException e){
            System.out.println("asd");
        }
        eb.clear();
    }

    /**
     *
     * @param channel TextChannel channel.
     * @param msg String message.
     * @param color Color color.
     */
    public static void messageLobbyList(TextChannel channel, String msg, Color color, String title, String footer){
        if(color != null){
            eb.setColor(color);
        }else{
            eb.setColor(Color.pink);
        }

        if(msg.length() != 0){
            eb.setDescription(msg);
        }else{
            eb.setDescription("NO MESSAGE PROVIDED");
        }

        if(title.length() != 0){
            eb.setTitle(title);
        }

        eb.setFooter("[" + getTime() + "] " + footer + " • provided by ArcticonSnipeBot", null);

        try{
            channel.sendMessage(eb.build()).queue(new Consumer<Message>()
            {
                @Override
                public void accept(Message m)
                {
                    Storage.setLobbylistMessage(channel.getGuild().getId(), m.getId());
                }
            });
        }catch (InsufficientPermissionException e){
            log(channel.getGuild().getName(), e.toString(), LogLevel.ERROR);
            //event.getTextChannel().sendMessage("**I NEED MORE PERMISSIONS!** Needed permission: EMBED_LINKS").complete();
        }

        eb.clear();
    }

    public static void dm(MessageReceivedEvent event, String msg){
        event.getAuthor().openPrivateChannel().queue((channel) ->
        {
            channel.sendMessage(msg).complete();
        });
    }

    public static void removeAllMessagesFromChannel(TextChannel channel){
        List<Message> msgs = channel.getHistory().retrievePast(100).complete();
        while(msgs.size() > 1){
            channel.deleteMessages(msgs).queue();
            msgs = channel.getHistory().retrievePast(100).complete();
        }
        if(msgs.size() == 1){
            Util.message(channel, "a", Color.white);
            msgs = channel.getHistory().retrievePast(2).complete();
            channel.deleteMessages(msgs).queue();
        }
    }

    public static void removeMessageFromTextchannel(String msgId, TextChannel channel){
        channel.deleteMessageById(msgId).queue();
    }

    public static EmbedBuilder getBuilder(){
        return eb;
    }
}
