package util;

public class Player {

    private String userid;

    public Player(String userid){
        this.userid = userid;
    }

    public String getUserId(){
        return userid;
    }

}
