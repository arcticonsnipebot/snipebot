package util;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class Secrets {

    //REAL
    private static final String token = "NDI3OTU5MTAyNzQ1MDgzOTE1.DZsHgQ._vp6MsB0_77_U8PyJfgfHAPd0FE";

    //TEST
//    private static final String token = "NDMyNjA4MTY3Mjc3MzYzMjEw.DavxTQ._WYF3GMDnw4wJ-OCIG6-q7A4xes";
    private static final String invoke = "+";














    private static final String authorID = "211113560598642688";

    private static final String version = "2.0.0";





    private static final String updatesFile ="updates.txt";
    private static final String updatesEndFile ="updatesEnd.txt";

    private static final String botchannelnameFile ="botchannelname.txt";
    private static final String listchannelnameFile ="listchannelname.txt";
    private static final String lobbychannelnameFile ="lobbychannelname.txt";
    private static final String serverSettingsFile ="serverSettings.txt";
    private static final String expireDatesFile ="expireDates.txt";
    private static final String acceptedSnipeBotRolesFile ="acceptedSnipeBotRoles.txt";
    private static final String last3AllowedRoleFile ="last3AllowedRole.txt";

    public static String getToken(){
        return token;
    }

    public static String getAuthorID(){
        return authorID;
    }

    public static String getVersion(){
        return version;
    }

    public static String getInvoke() {
        return invoke;
    }

    public static String getInvokeRegex() {
        return "\\" + invoke;
    }

    public static String getBotChannelname(MessageReceivedEvent event, String guildid) {
        return event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(guildid)).getName();
    }

    public static String getBotChannelId(String guildid) {
        return Storage.getBotChannelIdFromGuild(guildid);
    }

    public static String getLobbychannelname(MessageReceivedEvent event, String guildid) {
        return event.getGuild().getVoiceChannelById(Storage.getLobbyChannelIdFromGuild(guildid)).getName();
    }

    public static String getLobbychannelId(String guildid) {
        return Storage.getLobbyChannelIdFromGuild(guildid);
    }

    public static String getListchannelname(MessageReceivedEvent event, String guildid) {
        return event.getGuild().getTextChannelById(Storage.getLobbyListChannelIdFromGuild(guildid)).getName();
    }

    public static String getListchannelId(String guildid) {
        return Storage.getLobbyListChannelIdFromGuild(guildid);
    }

    public static String getUpdatesFile() {
        return updatesFile;
    }

    public static String getUpdatesEndFile() {
        return updatesEndFile;
    }

    public static String getBotchannelnameFile() {
        return botchannelnameFile;
    }

    public static String getListchannelnameFile() {
        return listchannelnameFile;
    }

    public static String getLobbychannelnameFile() {
        return lobbychannelnameFile;
    }

    public static String getServerSettingsFile() {
        return serverSettingsFile;
    }

    public static String getExpireDatesFile() {
        return expireDatesFile;
    }

    public static String getAcceptedSnipeBotRolesFile() {
        return acceptedSnipeBotRolesFile;
    }

    public static String getLast3AllowedRoleFile() {
        return last3AllowedRoleFile;
    }
}
