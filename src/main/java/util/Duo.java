package util;

import java.util.ArrayList;

public class Duo {

    private ArrayList<Player> players;

    public Duo(){
        players = new ArrayList<>();
    }

    public boolean setPlayer(int pos, String userid){
        if (players.size() < 2){
            boolean isPlayerInList = false;
            for (Player p : players){
                isPlayerInList = !p.getUserId().equals(userid);
            }
            if(!isPlayerInList){
                players.add(pos, new Player(userid));
            }
        }else{
            return false;
        }
        return true;
    }

    public Player getPlayer(int pos){
        return players.get(pos);
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
}
