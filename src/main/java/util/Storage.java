package util;

import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Storage {

    public enum Settings {
        BOTCHANNELID("botchannelid"),
        BOTCHANNELNAME("botchannelname"),
        LAST3CHANNELID("last3channelid"),
        LAST3CHANNELNAME("last3channelname"),
        LISTCHANNELID("listchannelid"),
        LISTCHANNELNAME("listchannelname"),
        LOBBYCHANNELID("lobbychannelid"),
        LOBBYCHANNELNAME("lobbychannelname"),
        INGAMECHANNELID("ingamechannelid"),
        INGAMECHANNELNAME("ingamechannelname"),
        CLANNAME("clanname"),
        MOVEUSER("moveuser"),
        MENTIONROLE("mentionrole"),
        POLL("poll"),
        EXPIRE("expire"),
        ROLE("role"),
        ROLES("roles"),
        LOCK("lock"),
        SOLO("solo"),
        DUO("duo"),
        SQUAD("squad"),
        HELP("help");

        private final String text;

        /**
         * @param text
         */
        Settings(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    //todo: add function to customize @here command

    //String GuildID, String botChannelID
    private static HashMap<String, String> botChannelNames = new HashMap<>();

    //String GuildID, String botChannelID
    private static HashMap<String, String> lobbyListChannelNames = new HashMap<>();

    //String GuildID, String botChannelID
    private static HashMap<String, String> lobbyChannelNames = new HashMap<>();

    //GuildID, MAP <CommandName, Value (int -- String)>
    private static HashMap<String, HashMap<String, String>> serverSettings = new HashMap<>();

    private static HashMap<String, Date> expireDates = new HashMap<>();

    private static HashMap<String, String> lobbylistMessage = new HashMap<>();

    private static HashMap<String, String> last3AllowedRole = new HashMap<>();


    //String GuildID, Role rolle
    private static HashMap<String, String> acceptedSnipeBotRoles = new HashMap<>();

    private static ObjectOutputStream objOutStream;
    private static ObjectInputStream objInStream;

    public Storage(){
    }

    public static HashMap<String, String> getBotChannelIds() {
        return botChannelNames;
    }

    public static HashMap<String, String> getLobbyListChannelIds() {
        return lobbyListChannelNames;
    }

    public static HashMap<String, String> getLobbyChannelIds() {
        return lobbyChannelNames;
    }

    public static HashMap<String, HashMap<String, String>> getServerSettings() {
        return serverSettings;
    }

    public static String getBotChannelIdFromGuild(String guildid) {
        try {
            return botChannelNames.get(guildid);
        }catch(NullPointerException e){
            return "";
        }
    }

    public static String getLobbyListChannelIdFromGuild(String guildid) {
        try {
            return lobbyListChannelNames.get(guildid);
        }catch(NullPointerException e){
            return "";
        }
    }

    public static String getLobbyChannelIdFromGuild(String guildid) {
        try {
            return lobbyChannelNames.get(guildid);
        }catch(NullPointerException e){
            return "";
        }
    }

    public static HashMap<String, String> getServerSettingsFromGuild(String guildid) {
        try {
            return serverSettings.get(guildid);
        }catch(NullPointerException e){
            return null;
        }
    }

    public static Date getExpireDateFromGuild(String guildid) {
        try {
            return expireDates.get(guildid);
        }catch(NullPointerException e){
            return null;
        }
    }

    public static String getAcceptedSnipeBotRoleFromGuild(String guildid) {
        try {
            return acceptedSnipeBotRoles.get(guildid);
        }catch(NullPointerException e){
            return null;
        }
    }

    public static String getLobbylistMessageFromGuild(String guildid) {
        try {
            return lobbylistMessage.get(guildid);
        }catch(NullPointerException e){
            return null;
        }
    }

    public static String getLast3AllowedRoleFromGuild(String guildid) {
        try {
            return last3AllowedRole.get(guildid);
        }catch(NullPointerException e){
            return null;
        }
    }

    public static boolean setBotChannelId(String guildId, String channelId) {
        if(!isChannelAlreadyInList(botChannelNames, guildId, channelId)){
            botChannelNames.put(guildId, channelId);
            saveStorage();
            return true;
        }
        return false;
    }

    public static boolean removeBotChannelId(String guildId, String channelId) {
        if(isChannelAlreadyInList(botChannelNames, guildId, channelId)){
            botChannelNames.remove(guildId, channelId);
            saveStorage();
            return true;
        }
        return false;
    }

    public static boolean setLobbyListChannelId(String guildId, String channelId) {
        if(!isChannelAlreadyInList(lobbyListChannelNames, guildId, channelId)){
            lobbyListChannelNames.put(guildId, channelId);
            saveStorage();
            return true;
        }
        return false;
    }

    public static boolean setLobbyChannelId(String guildId, String channelId) {
        if(!isChannelAlreadyInList(lobbyChannelNames, guildId, channelId)){
            lobbyChannelNames.put(guildId, channelId);
            saveStorage();
            return true;
        }
        return false;
    }

    public static boolean setServerSetting(String guildId, String key, String value) {
        try{
            if(serverSettings.get(guildId) != null || serverSettings.get(guildId).size() == 0){
                serverSettings.get(guildId).put(key, value);
            }
        }catch(NullPointerException e){
            HashMap<String, String> tmpHashmap = new HashMap<>();
            tmpHashmap.put(key, value);
            serverSettings.put(guildId, tmpHashmap);
        }
        saveStorage();
        return true;
    }

    public static boolean setExpireDate(String guildId, Date value) {
        try{
            if(expireDates != null || expireDates.size() == 0){
                expireDates.put(guildId, value);
            }
        }catch(NullPointerException e){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        saveStorage();
        return true;
    }

    public static boolean setAcceptedSnipeBotRole(String guildId, String value) {
        try{
            if(acceptedSnipeBotRoles != null){
                acceptedSnipeBotRoles.put(guildId, value);
            }
        }catch(NullPointerException e){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        saveStorage();
        return true;
    }

    public static boolean setLobbylistMessage(String guildId, String value) {
        try{
            if(lobbylistMessage != null){
                lobbylistMessage.put(guildId, value);
            }
        }catch(NullPointerException e){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        saveStorage();
        return true;
    }

    public static boolean setLast3AllowedRole(String guildId, String value) {
        try{
            if(last3AllowedRole != null){
                last3AllowedRole.put(guildId, value);
                return true;
            }
        }catch(NullPointerException e){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return false;
    }

    public static boolean isEverythingSet(TextChannel tc, String guildId){
        boolean tmp = false;

        try{
            if(getBotChannelIds().get(guildId) != null){
                tmp = true;
            }else{
                Util.message(tc, "BOT CHANNEL NOT SETUP", Color.red);
                tmp = false;
            }

            if(getLobbyListChannelIds().get(guildId) != null){
                tmp = true;
            }else{
                Util.message(tc, "LIST CHANNEL NOT SETUP", Color.red);
                tmp = false;
            }

            if(getLobbyChannelIds().get(guildId) != null){
                tmp = true;
            }else{
                Util.message(tc, "WAITING CHANNEL NOT SETUP", Color.red);
                tmp = false;
            }

        }catch(NullPointerException e){
            tmp = false;
        }
        return tmp;
    }

    private static boolean isChannelAlreadyInList(HashMap<String, String> hashmap, String guildId, String channelId){
        Iterator it = hashmap.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            if(pair.getKey().equals(guildId) && pair.getValue().equals(channelId)){
                return true;
            }
        }
        return false;
    }

    public static void initStorage(){
        File f = new File(Secrets.getBotchannelnameFile());
        if(f.exists() && !f.isDirectory()) {
            botChannelNames = readBotChannelIds();
            System.out.println("botChannelNames.toString()" + botChannelNames.toString());
        }else{
            System.err.println("FILE " + Secrets.getBotchannelnameFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        f = new File(Secrets.getListchannelnameFile());
        if(f.exists() && !f.isDirectory()) {
            lobbyListChannelNames = readLobbyListChannelIds();
            System.out.println("lobbyListChannelNames.toString()" + lobbyListChannelNames.toString());
        }else{
            System.err.println("FILE " + Secrets.getListchannelnameFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        f = new File(Secrets.getLobbychannelnameFile());
        if(f.exists() && !f.isDirectory()) {
            lobbyChannelNames = readLobbyChannelIds();
            System.out.println("lobbyChannelNames.toString()" + lobbyChannelNames.toString());
        }else{
            System.err.println("FILE " + Secrets.getLobbychannelnameFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        f = new File(Secrets.getServerSettingsFile());
        if(f.exists() && !f.isDirectory()) {
            serverSettings = readServerSettings();
            System.out.println("serverSettings.toString()" + serverSettings.toString());
        }else{
            System.err.println("FILE " + Secrets.getServerSettingsFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
                writeHashMapToFile(new HashMap<String, HashMap<String, String>>(), Secrets.getServerSettingsFile());
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        f = new File(Secrets.getExpireDatesFile());
        if(f.exists() && !f.isDirectory()) {
            expireDates = readExpireDates();
            System.out.println("expireDates.toString()" + expireDates.toString());
        }else{
            System.err.println("FILE " + Secrets.getExpireDatesFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        f = new File(Secrets.getAcceptedSnipeBotRolesFile());
        if(f.exists() && !f.isDirectory()) {
            acceptedSnipeBotRoles = readAcceptedSnipeBotRoles();
            System.out.println("acceptedSnipeBotRoles.toString()" + acceptedSnipeBotRoles.toString());
            System.out.println("acceptedSnipeBotRoles.size()" + acceptedSnipeBotRoles.size());
        }else{
            System.err.println("FILE " + Secrets.getAcceptedSnipeBotRolesFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        f = new File(Secrets.getLast3AllowedRoleFile());
        if(f.exists() && !f.isDirectory()) {
            last3AllowedRole = readLast3AllowedRole();
            System.out.println("acceptedSnipeBotRoles.toString()" + last3AllowedRole.toString());
            System.out.println("acceptedSnipeBotRoles.size()" + last3AllowedRole.size());
        }else{
            System.err.println("FILE " + Secrets.getLast3AllowedRoleFile() + " DOES NOT EXIST!");
            try {
                f.createNewFile();
            }catch(IOException e){
                Util.log(e.toString(), Util.LogLevel.ERROR);
            }
        }

        System.out.println("INIT STORAGE RDY!");
    }

    private static void saveStorage(){
        writeHashMapToFile(botChannelNames, Secrets.getBotchannelnameFile());
        writeHashMapToFile(lobbyListChannelNames, Secrets.getListchannelnameFile());
        writeHashMapToFile(lobbyChannelNames, Secrets.getLobbychannelnameFile());
        writeHashMapToFile(serverSettings, Secrets.getServerSettingsFile());
        writeHashMapToFile(expireDates, Secrets.getExpireDatesFile());
        writeHashMapToFile(acceptedSnipeBotRoles, Secrets.getAcceptedSnipeBotRolesFile());
        writeHashMapToFile(last3AllowedRole, Secrets.getLast3AllowedRoleFile());
    }

    private static HashMap<String, String> readBotChannelIds() {
        try {
            //reader = new BufferedReader(new FileReader(Secrets.getBotchannelnameFile()));
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getBotchannelnameFile()));
            HashMap<String, String> tmp = (HashMap<String, String>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException | ClassNotFoundException e ){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static HashMap<String, String> readLobbyListChannelIds() {
        //read from file

        try {
            //reader = new BufferedReader(new FileReader(Secrets.getBotchannelnameFile()));
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getListchannelnameFile()));
            HashMap<String, String> tmp = (HashMap<String, String>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException | ClassNotFoundException e ){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static HashMap<String, String> readLobbyChannelIds() {
        try {
            //reader = new BufferedReader(new FileReader(Secrets.getBotchannelnameFile()));
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getLobbychannelnameFile()));
            HashMap<String, String> tmp = (HashMap<String, String>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException | ClassNotFoundException e ){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static HashMap<String, HashMap<String, String>> readServerSettings() {
        try {
            //reader = new BufferedReader(new FileReader(Secrets.getBotchannelnameFile()));
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getServerSettingsFile()));
            HashMap<String, HashMap<String, String>> tmp = (HashMap<String, HashMap<String, String>>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException | ClassNotFoundException e ){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static HashMap<String, Date> readExpireDates() {
        try {
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getExpireDatesFile()));
            HashMap<String, Date> tmp = (HashMap<String, Date>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException | ClassNotFoundException e ){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static HashMap<String, String> readAcceptedSnipeBotRoles() {
        try {
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getAcceptedSnipeBotRolesFile()));
            HashMap<String, String> tmp = (HashMap<String, String>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException|ClassNotFoundException e){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static HashMap<String, String> readLast3AllowedRole() {
        try {
            objInStream = new ObjectInputStream(new FileInputStream(Secrets.getLast3AllowedRoleFile()));
            HashMap<String, String> tmp = (HashMap<String, String>)objInStream.readObject();
            objInStream.close();
            return tmp;
        }catch (IOException|ClassNotFoundException e){
            Util.log(e.toString(), Util.LogLevel.ERROR);
        }
        return new HashMap<>();
    }

    private static <T> boolean writeHashMapToFile(T obj, String filePath){
        try {
            objOutStream = new ObjectOutputStream(new FileOutputStream(filePath));
            objOutStream.writeObject(obj);
            objOutStream.close();
            return true;
        }catch (FileNotFoundException e){
            return false;
        }catch (IOException e){
            return false;
        }
    }

}
