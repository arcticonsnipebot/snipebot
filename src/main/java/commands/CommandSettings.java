package commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import util.Secrets;
import util.Storage;
import util.Util;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CommandSettings implements Command {

    private static EmbedBuilder eb = new EmbedBuilder();

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        try {
            if(!(event.getMember().getRoles().contains(event.getGuild().getRoleById(Storage.getAcceptedSnipeBotRoleFromGuild(event.getGuild().getId()))) || event.getAuthor().getId().equals(Secrets.getAuthorID()))){
                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**YOU ARE NOT AUTHORIZED!**", Color.red);
                return;
            }
        }catch(IllegalArgumentException e){
            //Util.message(event, "**YOU ARE NOT AUTHORIZED!**", Color.red);

        }

        if(args.length >= 1){
            switch (args[0]){
                case "set":
                    if (args.length >= 3){
                        if(args[1].equals(Storage.Settings.BOTCHANNELID.toString())){
                            if(Storage.setBotChannelId(event.getGuild().getId(), args[2])){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set Bot Channel to: " + event.getGuild().getTextChannelById(args[2]).getName(), Color.green);
                                return;
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET CHANNEL!", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.LISTCHANNELID.toString())){
                            if(Storage.setLobbyListChannelId(event.getGuild().getId(), args[2])){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set Lobby List Channel to: " + event.getGuild().getTextChannelById(args[2]).getName(), Color.green);
                                return;
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET CHANNEL!", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.LOBBYCHANNELID.toString())){
                            if(Storage.setLobbyChannelId(event.getGuild().getId(), args[2])){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set Lobby Channel to: " + event.getGuild().getVoiceChannelById(args[2]).getName(), Color.green);
                                return;
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET CHANNEL!", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.CLANNAME.toString())){
                            if(Storage.setServerSetting(event.getGuild().getId(), Storage.Settings.CLANNAME.toString(), args[2])){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set Clannames to :" + args[2], Color.green);
                                return;
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET SnipeMode to Squad!", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.MOVEUSER.toString())){
                            if(Storage.setServerSetting(event.getGuild().getId(), Storage.Settings.MOVEUSER.toString(), args[2])){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set MOVEUSER to: " + args[2], Color.green);
                                return;
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET MOVEUSER SETTING!", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.EXPIRE.toString())){
                            if(event.getAuthor().getId().equals(Secrets.getAuthorID())){
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                                    Storage.setExpireDate(args[2], sdf.parse(args[3]));
                                    event.getAuthor().openPrivateChannel().queue((channel) ->
                                    {
                                        channel.sendMessage("SET EXPIRE DATE TO: *" + args[3] + "*\tFOR SERVER: *" + event.getJDA().getGuildById(args[2]).toString() + "*").complete();
                                    });
                                }catch (ParseException e){
                                    Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**SOMETHING WENT WRONG WITH YOUR DATE**", Color.red);
                                }
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**YOU ARE NOT ALLOWED TO EXECUTE THIS COMMAND!**", Color.red);
                            }
                        }else if(args[1].equals(Storage.Settings.LOCK.toString())){
                            if(Storage.setLast3AllowedRole(event.getGuild().getId(), args[2])){
//                                Storage.setLast3AllowedRole()
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set lock Role to: " + event.getGuild().getRoleById(args[2]).getName(), Color.green);
//                                return;
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET ROLE!", Color.red);
//                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.ROLE.toString())){
                            if(Storage.setAcceptedSnipeBotRole(event.getGuild().getId(), args[2])){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Set accepted snipebot Role to: " + event.getGuild().getRoleById(args[2]).getName(), Color.green);
                            }else{
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "COULD NOT SET ROLE!", Color.red);
                            }
                        }
                    }
                    break;
                case "get":
                    if (args.length >= 2){
                        if(args[1].equals(Storage.Settings.BOTCHANNELNAME.toString())){
                            try{
                                if(!Storage.getBotChannelIdFromGuild(event.getGuild().getId()).equals("")){
                                    Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Bot Channel is set to: " + event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())).getName(), Color.white);
                                    return;
                                }
                            }catch (NullPointerException e){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**Bot Channel is not set!**", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.LISTCHANNELNAME.toString())){
                            try{
                                if(!Storage.getLobbyListChannelIdFromGuild(event.getGuild().getId()).equals("")){
                                    Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Lobby List Channel is set to: " + event.getGuild().getTextChannelById(Storage.getLobbyListChannelIdFromGuild(event.getGuild().getId())).getName(), Color.white);
                                    return;
                                }
                            }catch (NullPointerException e){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**Lobby List Channel is not set!**", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.LOBBYCHANNELNAME.toString())){
                            try{
                                if(!Storage.getLobbyChannelIdFromGuild(event.getGuild().getId()).equals("")){
                                    Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Lobby Channel is set to: " + event.getGuild().getVoiceChannelById(Storage.getLobbyChannelIdFromGuild(event.getGuild().getId())).getName(), Color.white);
                                    return;
                                }
                            }catch (NullPointerException e){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**Lobby Channel is not set!**", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.ROLE.toString())){
                            try{
                                if(!Storage.getAcceptedSnipeBotRoleFromGuild(event.getGuild().getId()).equals("")){
                                    Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "Role is set to: " + event.getGuild().getRoleById(Storage.getAcceptedSnipeBotRoleFromGuild(event.getGuild().getId())).getName(), Color.white);
                                    return;
                                }
                            }catch (NullPointerException e){
                                Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "**Role is not set!**", Color.red);
                                return;
                            }
                        }else if(args[1].equals(Storage.Settings.ROLES.toString())){
                            String output = "```Roles for Server: " + event.getGuild().getName() + "\n";
                            for (Role r : event.getGuild().getRoles()) {
                                //output += r.toString() + "\n";
                                output += "RoleId: " + r.getId() + ",\tRoleName: " + r.getName() + "\n";

                                if(output.length() >= 1800){
                                    output += "```";
                                    final String out = output;
                                    event.getAuthor().openPrivateChannel().queue((channel) ->
                                    {
                                        channel.sendMessage(out).complete();
                                    });
                                    output = "```";
                                }
                            }
                            output += "```";
                            final String out = output;
                            event.getAuthor().openPrivateChannel().queue((channel) ->
                            {
                                channel.sendMessage(out).complete();
                            });
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        if(success){
            Util.log(event.getGuild().getName(), Secrets.getInvoke() + "command settings called & executed!", Util.LogLevel.INFO, this.getClass());
        }else{
            Util.log(event.getGuild().getName(), Secrets.getInvoke() + "command settings called, but not executed!", Util.LogLevel.INFO, this.getClass());
        }
    }

    @Override
    public String getHelp() {
        return "**+settings set botchannelid <TextChannelID>:** Sets the Bot to listen to the given TextChannel" +
                "\n**+settings set listchannelid <TextChannelID>:** Sets the Bot to post the List of users in the Lobby to the given TextChannel" +
                "\n**+settings set lobbychannelid <VoiceChannelID>:** Sets the Bot to know which VoiceChannel is the Lobby Channel" +
                "\n**+settings set clanname <0/1>:** Sets the Bot to Squad Mode" +
                "\n**+settings set moveuser <0/1>:** Sets the Bot to move user after entering last 3 digits" +
                "\n**+settings set role <RoleId>:** Sets the accepted Role for the SnipeBot to execute Commands (get all Roles first!)" +
                "\n**+settings set lock <RoleId>:** //" +
                "\n\n**+settings get botchannelname:** Gets the TextChannel the Bot listens to" +
                "\n**+settings get listchannelname:** Gets the List of users TextChannel the Bot listens to" +
                "\n**+settings get lobbychannelname:** Gets the Lobby VoiceChannel the Bot listens to" +
                "\n**+settings get ingamechannelname:** Gets the Ingame VoiceChannel the Bot listens to" +
                "\n**+settings get role:** Gets the accepted SnipeBot Role" +
                "\n**+settings get roles:** Sends you a PM with all Server Roles, for setting the accepted SnipeBot Role";
    }
}
