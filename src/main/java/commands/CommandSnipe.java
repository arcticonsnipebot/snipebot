package commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.core.requests.RestAction;
import net.dv8tion.jda.core.utils.PermissionUtil;
import util.Duo;
import util.Secrets;
import util.Storage;
import util.Util;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;

public class CommandSnipe implements Command {

    //guildId, map (gamekey, userIDs)
    private static HashMap<String, HashMap<String, ArrayList<String>>> soloSnipeList = new HashMap<>();

    private static EmbedBuilder eb = new EmbedBuilder();

    //private static long
    private static long lastUpdated = System.currentTimeMillis();

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        if(Storage.isEverythingSet(event.getTextChannel(), event.getGuild().getId())){
            if(args.length >= 1){
                if(event.getTextChannel().canTalk()){
                    if(args[0].equals(Storage.Settings.SOLO.toString()) || args[0].equals(Storage.Settings.DUO.toString()) || args[0].equals(Storage.Settings.SQUAD.toString())){
                        startSnipes(event, args[0]);
                    }else if(args[0].equals(Storage.Settings.POLL.toString())){
                        sendGamemodePoll(event.getGuild().getTextChannelById(Secrets.getBotChannelId(event.getGuild().getId())));
                    }else{
                        Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), getHelp(), Color.white);
                    }
                }
            }
        }
    }

    public boolean isUserAllowedToUseCommand(Member m){
        return (m.getRoles().contains(m.getGuild().getRoleById(Storage.getAcceptedSnipeBotRoleFromGuild(m.getGuild().getId()))) || m.getUser().getId().equals(Secrets.getAuthorID()));
    }

    private void startSnipes(MessageReceivedEvent event, String mode){
//        TextChannel botchannel = event.getGuild().getTextChannelById(Secrets.getBotChannelId(event.getGuild().getId()));
        TextChannel lobbylist = event.getGuild().getTextChannelById(Secrets.getListchannelId(event.getGuild().getId()));

        if (isUserAllowedToUseCommand(event.getMember())){

            Storage.setLobbylistMessage(event.getGuild().getId(), null);
            Util.removeAllMessagesFromChannel(lobbylist);

            try {
                if(soloSnipeList.size() > 0){
                    soloSnipeList.get(event.getGuild().getId()).clear();
                }
            }catch(NullPointerException e){
//                Util.log(event.getGuild().getName(), e.toString(), Util.LogLevel.ERROR, this.getClass());
                e.printStackTrace();
            }
        }else{
            //Util.message(event, "You need this role: " + event.getGuild().getRoleById(Storage.getAcceptedSnipeBotRoleFromGuild(event.getGuild().getId())).getName(), Color.red);
            Util.dm(event, "You need this role: " + event.getGuild().getRoleById(Storage.getAcceptedSnipeBotRoleFromGuild(event.getGuild().getId())).getName());
            //Util.message(event, event.getAuthor().getAsMention() + " **YOU DONT HAVE PERMISSIONS TO DO THIS!**", Color.red);
            return;
        }

//        List<Message> msgs = botchannel.getHistory().retrievePast(100).complete();
//        if (msgs.size() == 1){
//            Util.message(botchannel, "", Color.white);
//            msgs = lobbylist.getHistory().retrievePast(100).complete();
//            lobbylist.deleteMessages(msgs).complete();
//        }else if(msgs.size() > 1){
//            lobbylist.deleteMessages(msgs).complete();
//        }



        Util.message(lobbylist, "**Gamemode** will be: **[" + mode.toUpperCase() + "]**!\nThere are currently " + getAmountOfUsersWaitingInLobby(event.getGuild()) + " players waiting to play snipes", Color.orange, "Next Snipe is starting in 1 Minute", "Initiated by " + event.getMember().getUser().getName());
        lockChannel(lobbylist);

//        event.getTextChannel().putPermissionOverride(event.getGuild().getPublicRole()).setDeny();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                Util.message(lobbylist, "Please enter your last 3 digits **NOW!**\nYou have 3 Minutes to enter your last 3 digits!", Color.pink, "Last 3 Digits", "Last 3");
                unlockChannel(lobbylist);

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        lockChannel(lobbylist);
                    }
                }, 3*60*1000);
            }
        }, 1*60*1000);
    }

    private void sendGamemodePoll(TextChannel channel){
        EmbedBuilder eb = Util.getBuilder();

        eb.setColor(Color.magenta);

        eb.setTitle("Poll for next Gamemode (open for 1 Minute)");

        eb.setFooter("[" + Util.getTime() + "] POLL • provided by ArcticonSnipeBot", null);

        eb.setDescription(":one: SOLO\n:two: DUO\n:four: SQUAD");

        try{
            channel.sendMessage(eb.build()).queue(new Consumer<Message>()
            {
                @Override
                public void accept(Message m)
                {
                    String lastMsgId = m.getId();
                    channel.addReactionById(lastMsgId, "\u0031\u20E3").queue();
                    channel.addReactionById(lastMsgId, "\u0032\u20E3").queue();
                    channel.addReactionById(lastMsgId, "\u0034\u20E3").queue();


                    eb.clear();

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            channel.getMessageById(lastMsgId).queue(new Consumer<Message>()
                            {
                                @Override
                                public void accept(Message m)
                                {
                                    String lastMsgId = m.getId();
                                    int solo = m.getReactions().get(0).getCount();
                                    int duo = m.getReactions().get(1).getCount();
                                    int squad = m.getReactions().get(2).getCount();

                                    String mode = "";

                                    if(solo > duo && solo > squad){
                                        mode = "solo";
                                    }else if(duo > solo && duo > squad){
                                        mode = "duo";
                                    }else if(squad > solo && squad > duo){
                                        mode = "squad";
                                    }else if(solo == 1 && duo == 1 && squad == 1){
                                        mode = "nobody voted";
                                    }else{
                                        mode = "host decides / new poll";
                                    }

                                    Util.message(channel, "Poll result: **" + mode.toUpperCase() + "**", Color.cyan, "Poll result", "POLL");

                                }
                            });
                        }
                    }, 1*60*1000);
                }
            });
        }catch (InsufficientPermissionException e){
            //event.getTextChannel().sendMessage("**I NEED MORE PERMISSIONS!** Needed permission: EMBED_LINKS").complete();
        }
        /*
            //0
            event.getMessage().addReaction("\u0030\u20E3").queue();
            //1
            event.getMessage().addReaction("\u0031\u20E3").queue();
            //2
            event.getMessage().addReaction("\u0032\u20E3").queue();
            //3
            event.getMessage().addReaction("\u0033\u20E3").queue();
            //4
            event.getMessage().addReaction("\u0034\u20E3").queue();
            //5
            event.getMessage().addReaction("\u0035\u20E3").queue();
            //6
            event.getMessage().addReaction("\u0036\u20E3").queue();
            //7
            event.getMessage().addReaction("\u0037\u20E3").queue();
            //8
            event.getMessage().addReaction("\u0038\u20E3").queue();
            //9
            event.getMessage().addReaction("\u0039\u20E3").queue();
         */



    }

    private void lockChannel(TextChannel channel){
        Role r = channel.getGuild().getRoleById(Storage.getLast3AllowedRoleFromGuild(channel.getGuild().getId()));

        channel.putPermissionOverride(r).setAllow(66560).queue();
        Util.message(channel, "*Chat locked.*");
    }

    private void unlockChannel(TextChannel channel){
        Role r = channel.getGuild().getRoleById(Storage.getLast3AllowedRoleFromGuild(channel.getGuild().getId()));

        channel.putPermissionOverride(r).setAllow(68608).queue();
    }

    private boolean isUserAlreadyInLobby(MessageReceivedEvent event){

        Iterator it = soloSnipeList.get(event.getGuild().getId()).entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            ArrayList<String> playersInGame = (ArrayList<String>)pair.getValue();
            for (String userid : playersInGame ) {
                if (userid.equals(event.getAuthor().getId())){
                    return true;
                }
            }
        }
        return false;
    }

    private void moveUserToLobby(Member user, Guild guild){
        guild.getController().moveVoiceMember(user, guild.getVoiceChannelById(Secrets.getLobbychannelId(guild.getId()))).complete();
    }

    public void registerUserInGame(String gamekey, MessageReceivedEvent event){
        Guild g = event.getGuild();

        gamekey = gamekey.toLowerCase();

        ArrayList<String> tmpArrList = new ArrayList<>();
        tmpArrList.add(event.getAuthor().getId());

        HashMap<String, ArrayList<String>> tmpHashMap = new HashMap<>();
        tmpHashMap.put(gamekey, tmpArrList);

        boolean newGameKeyButNotNewArraylist = false;

        boolean overwrite = false;

        try {
            if(soloSnipeList.get(g.getId()) != null || soloSnipeList.get(g.getId()).size() == 0){

                if(!isUserAlreadyInLobby(event)){

                    newGameKeyButNotNewArraylist = true;

                    soloSnipeList.get(g.getId()).get(gamekey).add(event.getAuthor().getId());

//                    Util.message(event, "Registered User : " + event.getAuthor().getAsMention()
//                            + " in Game: **" + gamekey + "**", Color.green);

                    Util.removeMessageFromTextchannel(event.getMessageId(), event.getTextChannel());


                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            printLobbyList(event);
                        }
                    }, 3000);

                    return;
                }else{
                    Iterator it = soloSnipeList.get(g.getId()).entrySet().iterator();

                    while (it.hasNext()){
                        //HashMap<String, ArrayList<String>>
                        Object n = it.next();
                        //HashMap<String, ArrayList<String>> next = (HashMap<String, ArrayList<String>>)n;

                        //Map.Entry pair = (Map.Entry)it.next();
                        Map.Entry pair = (Map.Entry)n;
                        ArrayList<String> map = (ArrayList<String>)pair.getValue();

                        if(map.contains(event.getAuthor().getId())){

                            map.remove(event.getAuthor().getId());

                            //if (soloSnipeList.get(g.getId()).equals(map))

                            if(soloSnipeList.get(event.getGuild().getId()).get(gamekey) != null){
                                soloSnipeList.get(event.getGuild().getId()).get(gamekey).add(event.getAuthor().getId());
                            }else{
                                ArrayList<String> tmpArrList2 = new ArrayList<>();
                                tmpArrList2.add(event.getAuthor().getId());
                                soloSnipeList.get(g.getId()).put(gamekey, tmpArrList2);
                            }

//                            Util.message(event, "Registered User : " + event.getAuthor().getAsMention()
//                                    + " in Game: **" + gamekey + "**", Color.green);

                            Util.removeMessageFromTextchannel(event.getMessageId(), event.getTextChannel());

                            if (map.size() == 0){
                                soloSnipeList.get(g.getId()).remove((String)pair.getKey(), (ArrayList<String>)pair.getValue());
                            }

                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    printLobbyList(event);
                                }
                            }, 3000);
                            return;
                        }
                    }
                    return;
                }
            }
            soloSnipeList.put(g.getId(), tmpHashMap);

//            Util.message(event, "Registered User : " + event.getAuthor().getAsMention()
//                    + " in Game: **" + gamekey + "**", Color.green);

            Util.removeMessageFromTextchannel(event.getMessageId(), event.getTextChannel());

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    printLobbyList(event);
                }
            }, 3000);

            return;
        }catch (NullPointerException e){
            if(newGameKeyButNotNewArraylist){
                //new arraylist

                soloSnipeList.get(g.getId()).put(gamekey, tmpArrList);

//                Util.message(event, "Registered User : " + event.getAuthor().getAsMention()
//                        + " in Game: **" + gamekey + "**", Color.green);

                Util.removeMessageFromTextchannel(event.getMessageId(), event.getTextChannel());


                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        printLobbyList(event);
                    }
                }, 3000);

                return;
            }else{
                soloSnipeList.put(g.getId(), tmpHashMap);

//                Util.message(event, "Registered User : " + event.getAuthor().getAsMention()
//                        + " in Game: **" + gamekey + "**", Color.green);

                Util.removeMessageFromTextchannel(event.getMessageId(), event.getTextChannel());

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        printLobbyList(event);
                    }
                }, 3000);

                return;
            }
        }
    }

    private static void printLobbyList(MessageReceivedEvent event){
        if((System.currentTimeMillis()-lastUpdated) >= 2000){

            TextChannel tc = event.getGuild().getTextChannelById(Secrets.getListchannelId(event.getGuild().getId()));
//            String lastMessageID = event.getGuild().getTextChannelById(Secrets.getListchannelId(event.getGuild().getId())).getLatestMessageId();
            String listMsgId = Storage.getLobbylistMessageFromGuild(event.getGuild().getId());

            String snipeList = getSnipeListAsString(event);

            try {

                if (listMsgId != null){
                    Util.editMessage(tc, listMsgId,  snipeList, Color.white, "Current Snipe: ", getAmoutOfUsersInSnipe(event.getGuild()) + " Users in " + getAmoutOfLobbiesInSnipe(event.getGuild()) + " Lobbies");
                }else{
                    Util.messageLobbyList(tc, snipeList, Color.white, "Current Snipe: ", getAmoutOfUsersInSnipe(event.getGuild()) + " Users in " + getAmoutOfLobbiesInSnipe(event.getGuild()) + " Lobbies");
                }

//                if(soloSnipeList.get(event.getGuild().getId()).size() > 0){
//                    try{
//                        Util.message(tc, snipeList, Color.white, "Gamemode: ", getAmoutOfUsersInSnipe(event.getGuild()) + " Users in " + getAmoutOfLobbiesInSnipe(event.getGuild()) + " Lobbies • provided by ArcticonSnipeBot");
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }else{
//                    Util.editMessage(tc, listMsgId,  "", Color.white, "Userlist: ", "• provided by ArcticonSnipeBot");
//                }

            }catch (Exception e){
//                Util.messageLobbyList(tc, "a", Color.white, "Userlist: ", null);
//                Util.editMessage(tc, listMsgId,  "", Color.white, "Userlist: ", "• provided by ArcticonSnipeBot");

            }
            lastUpdated = System.currentTimeMillis();
        }
    }

    private static String getSnipeListAsString(MessageReceivedEvent event){
        try{
            String output = "";

//            boolean clanname = Storage.getServerSettingsFromGuild(event.getGuild().getId()).get(Storage.Settings.CLANNAME.toString()).equals("1");

            Iterator it = soloSnipeList.get(event.getGuild().getId()).entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                ArrayList<String> playersInGame = (ArrayList<String>)pair.getValue();

                output += "**" + pair.getKey() + "** (" + playersInGame.size() + ")\n";

                for (String userid : playersInGame ) {

                    //output += "\t" + event.getTextChannel().getJDA().getUserById(userid).getName() + "\n";
                    String username;

//                if(clanname){
//                    username = event.getGuild().getMember(event.getGuild().getTextChannelById(Secrets.getListchannelId(event.getGuild().getId())).getJDA().getUserById(userid)).getEffectiveName().split(" ")[0];
//                }else{
//                    username = event.getGuild().getMember(event.getGuild().getTextChannelById(Secrets.getListchannelId(event.getGuild().getId())).getJDA().getUserById(userid)).getEffectiveName();
//                    if(username.equals("null")){
//                        username = event.getTextChannel().getJDA().getUserById(userid).getName();
//                    }
//                }

                    username = event.getTextChannel().getJDA().getUserById(userid).getAsMention();

                    output += "\t" + username + "\n";
                }
                output += "\n\n";
            }
            output += "";

            return output;
        }catch (Exception e){
            return "";
        }

    }

    private static int getAmoutOfUsersInSnipe(Guild guild){
        int count = 0;
        Iterator it = soloSnipeList.get(guild.getId()).entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            ArrayList<String> playersInGame = (ArrayList<String>)pair.getValue();

            for (String userid : playersInGame ) {
                count++;
            }
        }
        return count;
    }

    private static int getAmoutOfLobbiesInSnipe(Guild guild){
        int count = 0;
        for(Map.Entry<String, ArrayList<String>> entry : soloSnipeList.get(guild.getId()).entrySet()) {
            count++;
        }
        return count;
    }

    private static int getAmountOfUsersWaitingInLobby(Guild g){
        return g.getVoiceChannelById(Secrets.getLobbychannelId(g.getId())).getMembers().size();
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        if(success){
            Util.log(event.getGuild().getName(), Secrets.getInvoke() + "command snipe called & executed!", Util.LogLevel.INFO, this.getClass());
        }else{
            Util.log(event.getGuild().getName(), Secrets.getInvoke() + "command snipe called, but not executed!", Util.LogLevel.INFO, this.getClass());
        }
    }

    @Override
    public String getHelp() {
        return "**+s solo**: Start Solo Snipe Lobby" +
                "\n**+s duo**: Start Duo Snipe Lobby" +
                "\n**+s squad**: Start Squad Snipe Lobby" +
                "\n**+s poll**: Start a Poll";
    }
}
