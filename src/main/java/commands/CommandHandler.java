package commands;

import java.util.HashMap;

public class CommandHandler {

    private static final CommandParser parser = new CommandParser();
    private static HashMap<String, Command> commands = new HashMap<>();

    public static void handleCommand(CommandContainer cmdContainer) {

        if (commands.containsKey(cmdContainer.getInvoke())) {
            boolean called = commands.get(cmdContainer.getInvoke()).called(cmdContainer.getArgs(), cmdContainer.getEvent());
            if (!called) {
                commands.get(cmdContainer.getInvoke()).action(cmdContainer.getArgs(), cmdContainer.getEvent());
            }
            commands.get(cmdContainer.getInvoke()).executed(!called, cmdContainer.getEvent());
        }
    }

    public static CommandParser getParser() {
        return parser;
    }

    public static HashMap<String, Command> getCommands() {
        return commands;
    }

    public static HashMap<String, Command> getCommandsSorted() {
        //todo:
        return null;
    }

}
