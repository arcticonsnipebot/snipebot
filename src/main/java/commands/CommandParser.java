package commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import util.Secrets;

import java.util.ArrayList;

public class CommandParser {

    public CommandContainer parseCommand(String raw, MessageReceivedEvent event){

        String beheaded = raw.replaceFirst(Secrets.getInvokeRegex(), "");
        String[] splitBeheaded = beheaded.split(" ");
        String invoke = splitBeheaded[0];
        ArrayList<String> split = new ArrayList<>();
        for (String s : splitBeheaded) {
            split.add(s);
        }
        String[] args = new String[split.size() - 1];
        split.subList(1, split.size()).toArray(args);

        return new CommandContainer(raw, beheaded, splitBeheaded, invoke, args, event);
    }

}
