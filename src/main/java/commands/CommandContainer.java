package commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class CommandContainer {

    private final String raw;
    private final String beheaded;
    private final String[] splitBeheaded;
    private final String invoke;
    private final String[] args;
    private final MessageReceivedEvent event;

    public CommandContainer(String raw, String beheaded, String[] splitBeheaded, String invoke, String[] args, MessageReceivedEvent event) {
        this.raw = raw;
        this.beheaded = beheaded;
        this.splitBeheaded = splitBeheaded;
        this.invoke = invoke;
        this.args = args;
        this.event = event;
    }

    public String getRaw(){
        return raw;
    }

    public String getBeheaded() {
        return beheaded;
    }

    public String[] getSplitBeheaded() {
        return splitBeheaded;
    }

    public String getInvoke() {
        return invoke;
    }

    public String[] getArgs() {
        return args;
    }

    public MessageReceivedEvent getEvent() {
        return event;
    }

}
