package commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import util.Secrets;
import util.Storage;
import util.Util;

import java.awt.*;
import java.util.Map;

public class CommandCommands implements Command {

    @Override
    public boolean called(String[] args, MessageReceivedEvent event) {
        return false;
    }

    @Override
    public void action(String[] args, MessageReceivedEvent event) {
        String out = "";
        if(event.getTextChannel().canTalk()){
            for (Map.Entry<String, Command> cmd : CommandHandler.getCommands().entrySet()) {
                out += cmd.getValue().getHelp() + "\n";
            }
        }

        Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), out, Color.white, "Commands:", "");
    }

    @Override
    public void executed(boolean success, MessageReceivedEvent event) {
        if(success){
            Util.log(event.getGuild().getName(), Secrets.getInvoke() + "command commands called & executed!", Util.LogLevel.INFO, this.getClass());
        }else{
            Util.log(event.getGuild().getName(), Secrets.getInvoke() + "command commands called, but not executed!", Util.LogLevel.INFO, this.getClass());
        }
    }

    @Override
    public String getHelp() {
        return "**+commands:** List all commands";
    }
}
