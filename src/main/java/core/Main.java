package core;

import commands.CommandCommands;
import commands.CommandHandler;
import commands.CommandSettings;
import commands.CommandSnipe;
import listeners.MessageListener;
import listeners.ReadyListener;
import listeners.TextchannelListener;
import listeners.VoiceChannelListener;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import util.Secrets;
import util.Storage;

import javax.security.auth.login.LoginException;

public class Main {

    private static JDABuilder builder;

    public static void main(String[] args) {
        builder = new JDABuilder(AccountType.BOT);

        builder.setToken(Secrets.getToken());
        builder.setAutoReconnect(true);

        builder.setStatus(OnlineStatus.ONLINE);

        Storage.initStorage();

        initListeners();

        initCommands();

        try {
            JDA jda = builder.buildBlocking();
        } catch (LoginException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void initListeners(){
        builder.addEventListener(new ReadyListener());
        builder.addEventListener(new TextchannelListener());
        builder.addEventListener(new VoiceChannelListener());
        builder.addEventListener(new MessageListener());
    }

    private static void initCommands(){
        CommandHandler.getCommands().put("commands", new CommandCommands());
        CommandHandler.getCommands().put("s", new CommandSnipe());
        CommandHandler.getCommands().put("settings", new CommandSettings());
    }

}
