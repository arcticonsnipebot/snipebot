package listeners;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import util.Secrets;
import util.Storage;
import util.Util;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadyListener extends ListenerAdapter {

    public void onReady(ReadyEvent e){
        System.out.println("READY");

        String out = "\nThis bot is running on following servers: \n";

        for (Guild g : e.getJDA().getGuilds() ) {
            out += g.getName() + " (" + g.getId() + ") \n";
        }

        System.out.println(out);

        String updates = "";
        String updatesEnd = "";

        try {
            updates = new String(Files.readAllBytes(Paths.get(Secrets.getUpdatesFile())));
        } catch (IOException ex) {
            updates = "NO UPDATES!";
            Util.log(updates, Util.LogLevel.INFO, this.getClass());
            Util.log(ex.toString(), Util.LogLevel.ERROR, this.getClass());
        }

        try {
            updatesEnd = new String(Files.readAllBytes(Paths.get(Secrets.getUpdatesEndFile())));
        } catch (IOException ex) {
            updatesEnd = "NO UPDATES!";
            Util.log(updatesEnd, Util.LogLevel.INFO, this.getClass());
            Util.log(ex.toString(), Util.LogLevel.ERROR, this.getClass());
        }

        for (Guild g : e.getJDA().getGuilds() ) {
            try{
                Util.message(g.getTextChannelById(Secrets.getBotChannelId(g.getId())), "BEEP BOOP. Seems like i just restarted! (╯°□°）╯︵ ┻━┻", Color.blue);

                if(updates.length() >= 2){
                    Util.message(g.getTextChannelById(Secrets.getBotChannelId(g.getId())), "Here is a list of the new updates in Version " + Secrets.getVersion() +
                            ":\n" + updates + "\n" + updatesEnd, Color.white);
                }

                if(Storage.getAcceptedSnipeBotRoleFromGuild(g.getId()) == null){
                    Storage.setAcceptedSnipeBotRole(g.getId(), g.getPublicRole().getId());
                }
            }catch (IllegalArgumentException ex){

            }

        }
    }

}
