package listeners;

import net.dv8tion.jda.core.events.channel.text.TextChannelDeleteEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import util.Secrets;
import util.Storage;

public class TextchannelListener extends ListenerAdapter{

    public void onTextChannelDelete(TextChannelDeleteEvent event){
        if(event.getChannel().getId().equals(Secrets.getBotChannelId(event.getGuild().getId()))){
            Storage.removeBotChannelId(event.getGuild().getId(), event.getChannel().getId());
        }
    }
}
