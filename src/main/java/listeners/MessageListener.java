package listeners;

import commands.CommandHandler;
import commands.CommandSnipe;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import util.Secrets;
import util.Storage;
import util.Util;

import java.awt.*;
import java.util.Date;

public class MessageListener extends ListenerAdapter {

    public void onMessageReceived(MessageReceivedEvent event) {
        if(!event.getAuthor().isBot()){
            if(event.getMessage().getContentRaw().startsWith(Secrets.getInvoke()) && event.getTextChannel().getId().equals(Storage.getBotChannelIdFromGuild(event.getGuild().getId()))){
                try{
                    if(new Date().after(Storage.getExpireDateFromGuild(event.getGuild().getId()))){
                        if(!(event.getAuthor().getId().equals(Secrets.getAuthorID()) || event.getAuthor().getId().equals("432608167277363210"))){
                            Util.message(event.getGuild().getTextChannelById(Storage.getBotChannelIdFromGuild(event.getGuild().getId())), "BOT EXPIRED", Color.red);
                            return;
                        }
                    }
                }catch (NullPointerException e){
//                Util.message(event, "NO EXPIRATIONDATE SET!", Color.red);
//                return;
                }

                try{
                    if ((!event.getMessage().getAuthor().getId().equals(event.getJDA().getSelfUser().getId()) || event.getTextChannel().equals(event.getGuild().getTextChannelById(Secrets.getBotChannelId(event.getGuild().getId()))))) {
                        CommandHandler.handleCommand(CommandHandler.getParser().parseCommand(event.getMessage().getContentRaw(), event));
                    }
                }catch (IllegalArgumentException e){
//            CommandHandler.handleCommand(CommandHandler.getParser().parseCommand(event.getMessage().getContentRaw(), event));
                    e.printStackTrace();
                }
            }else if(event.getMessage().getContentRaw().matches("^[a-zA-Z0-9]{3}") && event.getTextChannel().getId().equals(Secrets.getListchannelId(event.getGuild().getId()))){
                new CommandSnipe().registerUserInGame(event.getMessage().getContentRaw(), event);
            }else if(event.getTextChannel().getId().equals(Storage.getLobbyListChannelIdFromGuild(event.getGuild().getId()))){
                Util.removeMessageFromTextchannel(event.getMessageId(), event.getTextChannel());
            }else if(event.getMember().getUser().getId().equals(Secrets.getAuthorID())){
                CommandHandler.handleCommand(CommandHandler.getParser().parseCommand(event.getMessage().getContentRaw(), event));
            }
        }
    }

}
